import * as React from "react";
import { View } from "react-native";

import { Card, Title, Button, Divider } from "react-native-paper";
export default function DetailsItemCard({
  itemClass,
  subClass,
  uri,
  designer,
  wall,
  week,
  market,
  heelHeight,
}) {
  return (
    <View style={{ padding: 10 }}>
      <Card style={{ width: 250 }}>
        <Card.Title title={itemClass} subtitle={designer} />
        <Card.Content>
          <View style={{ flexDirection: "row", flex: 1 }}>
            <Title> {wall} </Title>
            <Divider />
            <Title> {"Week " + week} </Title>
            <Divider />
            <Title> {market} </Title>
          </View>
          <View style={{ flexDirection: "row", flex: 1 }}>
            <Title> {itemClass} </Title>
            <Divider />
            <Title> {subClass} </Title>
          </View>
          <Card.Cover style={{ height: 200 }} source={{ uri: uri }} />
          <View style={{ flexDirection: "row", flex: 1 }}>
            <Title> {"Heel range: " + heelHeight} </Title>
          </View>
          <Card.Actions>
            <Button>Update</Button>
          </Card.Actions>
        </Card.Content>
      </Card>
    </View>
  );
}
