import React from "react";
import { View, FlatList } from "react-native";
import { Title } from "react-native-paper";
import DetailsItemCard from "./DetailsItemCard";
import { TouchableOpacity } from "react-native";
const shoes = require("../data/shoes.json");
export default function DetailsView() {
  return (
    <View style={{ margin: 30, height: 470, flex: 1 }}>
      <FlatList
        horizontal={true}
        showsHorizontalScrollIndicator={true}
        data={shoes}
        renderItem={({ item }) => (
          <TouchableOpacity>
            <DetailsItemCard
              itemClass={item.class}
              subClass={item.subClass}
              uri={item.image}
              designer={item.designer}
              wall={item.wall}
              week={item.week}
              market={item.market}
              heelHeight={item.heelHeight}
            />
          </TouchableOpacity>
        )}
        keyExtractor={(item) => `${item.id}`}
      />
    </View>
  );
}
